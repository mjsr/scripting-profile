﻿Get-Process autohotkey | kill
$ahkPath = '~\scpro\ahk'
ls $ahkPath -Filter *.ahk | % {
    & $_.FullName;
    Write-Host -ForegroundColor Yellow "$_ Executed"}