Function ReplaceInFiles($pattern, $replace, $file, $directory, $filter, [switch]$recurse)
{
    function ReplaceInFile($filePath, $pattern, $replace)
    {
        $text = [IO.File]::ReadAllText($filePath)
        if([Regex]::IsMatch($text, $pattern))
        {
            $newText = [Regex]::Replace($text, $pattern, $replace)
            [IO.File]::WriteAllText($filePath, $newText)
            Write-Host "File modified: $filePath"
        }
    }

    if($file -ne $null)
    {
        $filePath = (Get-ChildItem $file).FullName
        ReplaceInFile $filePath $pattern $replace
    }

    if($directory -ne $null)
    {
        if($recurse){
            if($filter -ne $null)
            {
                Get-ChildItem $directory -Filter $filter -Recurse -File | % { ReplaceInfile $_.FullName $pattern $replace}
            }
            else
            {
                Get-ChildItem $directory -Recurse -File | % { ReplaceInfile $_.FullName $pattern $replace}
            }
        }
        else {
            if($filter -ne $null)
            {
                Get-ChildItem $directory  -file -Filter -Filter $filter| % { ReplaceInfile $_.FullName $pattern $replace}
            }
            else
            {
                Get-ChildItem $directory -Recurse -File | % { ReplaceInfile $_.FullName $pattern $replace}
            }
        }
    }
}

