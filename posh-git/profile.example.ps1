Push-Location (Split-Path -Path $MyInvocation.MyCommand.Definition -Parent)

# Load posh-git module from current directory
Import-Module .\posh-git

# If module is installed in a default location ($env:PSModulePath),
# use this instead (see about_Modules for more information):
# Import-Module posh-git


# Set up a simple prompt, adding the git prompt parts inside git repos
function global:prompt {
    $realLASTEXITCODE = $LASTEXITCODE

    # Reset color, which can be messed up by Enable-GitColors
    $Host.UI.RawUI.ForegroundColor = $GitPromptSettings.DefaultForegroundColor
    $Host.UI.RawUI.BackgroundColor = "Black";
    #Write-Host($pwd.ProviderPath) -nonewline

    write-host (get-date -format "h:m:s tt ") -ForegroundColor White -NoNewline

    write-host (Split-Path $pwd -Leaf) -ForegroundColor Green -NoNewline

    Write-VcsStatus

    $Host.UI.RawUI.WindowTitle = [IO.Path]::GetFileName((Get-Location).Path).ToUpper()

    $global:LASTEXITCODE = $realLASTEXITCODE
    return "> "
}

Enable-GitColors

Pop-Location

Start-SshAgent -Quiet
