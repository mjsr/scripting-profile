param($file)
$pass = Read-Host "Pass" -AsSecureString
$baseName = [IO.Path]::GetFileNameWithoutExtension($file)
$null=(7z x "$file" -p$pass)
while(-not(test-path "$baseName.txt"))
{
    sleep  2;
}
Get-Content "$baseName.txt"
Remove-Item "$baseName.txt"
