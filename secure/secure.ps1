param($file)
$pass = Read-Host "Pass" -AsSecureString
$newName = [IO.Path]::GetFileNameWithoutExtension($file) + ".7z"
$null=(7z a $newName $file -p$pass)
while(-not(test-path $newName))
{
    sleep  2;
}
Remove-Item $file
"Secured"